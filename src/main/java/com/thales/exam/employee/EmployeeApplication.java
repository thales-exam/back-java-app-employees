package com.thales.exam.employee;

import io.swagger.v3.oas.models.ExternalDocumentation;
import io.swagger.v3.oas.models.OpenAPI;
import io.swagger.v3.oas.models.info.Contact;
import io.swagger.v3.oas.models.info.Info;
import io.swagger.v3.oas.models.info.License;
import org.springdoc.core.GroupedOpenApi;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;

@SpringBootApplication
public class EmployeeApplication {

    public static void main(String[] args) {
        SpringApplication.run(EmployeeApplication.class, args);
    }

    @Bean
    public GroupedOpenApi publicApi() {
        return GroupedOpenApi.builder()
                .group("thales-apis")
                .packagesToScan("com.thales.exam.employee")
                //.pathsToMatch("/api/**")
                .build();
    }

    @Bean
    public OpenAPI springShopOpenAPI() {

        OpenAPI openAPI = new OpenAPI();

        openAPI.info(new Info()
                .title("Thales exam API")
                .description("Thales exam employees API reference for developers, teh error handler is implement using \n"
                        + " The IETF devised RFC 7807 effor, which creates a generalized error-handling schema.\n"
                        + "https://tools.ietf.org/html/rfc7807")
                .version("v0.0.1")
                .license(new License()
                        .name("Apache 2.0")
                        .url("http://springdoc.org"))
                .contact(new Contact()
                        .name("Fabián Millán")
                        .email("fabiandresmillanj@gmail.com")
                        .url("https://google.com")));

        openAPI.externalDocs(new ExternalDocumentation()
                .description("SpringShop Wiki Documentation")
                .url("https://springshop.wiki.github.org/docs"));

        /*openAPI.components(new Components()
                //HTTP Basic, see: https://swagger.io/docs/specification/authentication/basic-authentication/
                .addSecuritySchemes("basicScheme", new SecurityScheme()
                        .type(SecurityScheme.Type.HTTP)
                        .scheme("basic")
                )
                //API Key, see: https://swagger.io/docs/specification/authentication/api-keys/
                .addSecuritySchemes("apiKeyScheme", new SecurityScheme()
                        .type(SecurityScheme.Type.APIKEY)
                        .in(SecurityScheme.In.HEADER)
                        .name("X-API-KEY")
                )
                //OAuth 2.0, see: https://swagger.io/docs/specification/authentication/oauth2/
                .addSecuritySchemes("oAuthScheme", new SecurityScheme()
                        .type(SecurityScheme.Type.OAUTH2)
                        .description("This API uses OAuth 2 with the implicit grant flow. [More info](https://api.example.com/docs/auth)")
                        .flows(new OAuthFlows()
                                .implicit(new OAuthFlow()
                                        .authorizationUrl("https://api.example.com/oauth2/authorize")
                                        .scopes(new Scopes()
                                                .addString("read_pets", "read your pets")
                                                .addString("write_pets", "modify pets in your account")
                                        )
                                )
                        )
                )
                .addSecuritySchemes("bearerAuthScheme", new SecurityScheme()
                        .name("bearerAuth")
                        .type(SecurityScheme.Type.HTTP)
                        .scheme("bearer")
                        .bearerFormat("JWT")
                )
        );
        openAPI.addSecurityItem(new SecurityRequirement()
                .addList("basicScheme").addList("apiKeyScheme")
        );
        openAPI.addSecurityItem(new SecurityRequirement()
                .addList("oAuthScheme")
        );
        openAPI.addSecurityItem(new SecurityRequirement()
                .addList("bearerAuthScheme"));
         */
        return openAPI;
    }
}
