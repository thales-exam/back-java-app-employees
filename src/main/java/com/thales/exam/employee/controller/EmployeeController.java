/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.thales.exam.employee.controller;

import com.thales.exam.employee.dto.Employee;
import com.thales.exam.employee.dto.EmployeeResponseDto;
import com.thales.exam.employee.dto.mapper.EmployeeResponseMapper;
import com.thales.exam.employee.service.EmployeeService;
import io.swagger.v3.oas.annotations.tags.Tag;
import java.io.IOException;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.bind.annotation.RequestMapping;
import java.util.List;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;

/**
 *
 * @author famillan
 */
@Slf4j
@CrossOrigin
@RestController
@RequestMapping("/api/employees")
@Tag(name = "Employees API", description = "Api of managment all thales's employees")
public class EmployeeController {

    @Autowired
    private EmployeeService employeeService;

    @Autowired
    private EmployeeResponseMapper responseMapper;

    @GetMapping()
    public ResponseEntity<List<EmployeeResponseDto>> list() {
        try {
            List<Employee> empoEmployees = employeeService.getEmployees();
            if (empoEmployees != null) {
                List<EmployeeResponseDto> response = responseMapper.entityListToModelResposeList(empoEmployees);
                return ResponseEntity.ok(response);
            }
        } catch (IOException ex) {
            log.error(ex.getMessage());
            return ResponseEntity.status(HttpStatus.SERVICE_UNAVAILABLE).body(null);
        } catch (Error ex) {
            log.error(ex.getMessage());
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(null);
        }
        return ResponseEntity.noContent().build();
    }

    @GetMapping("/{id}")
    public ResponseEntity<EmployeeResponseDto> get(@PathVariable Long id) {
        try {
            Employee employee = employeeService.getEmployeeById(id);
            if (employee != null) {
                EmployeeResponseDto response = responseMapper.entityToModelRespose(employee);
                return ResponseEntity.ok(response);
            }
        } catch (IOException ex) {
            log.error(ex.getMessage());
            return ResponseEntity.status(HttpStatus.SERVICE_UNAVAILABLE).body(null);
        } catch (Error ex) {
            log.error(ex.getMessage());
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(null);
        }
        return ResponseEntity.notFound().build();
    }

}
