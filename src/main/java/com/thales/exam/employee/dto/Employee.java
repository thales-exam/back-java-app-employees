/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.thales.exam.employee.dto;

import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Data;

/**
 *
 * @author famillan
 */
@Data
public class Employee {

    private Long id;

    private String employeeName;

    private Integer employeeSalary;

    private Integer employeeAge;

    private String profileImage;

    private Integer employeeAnualSalary;

}
