/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.thales.exam.employee.dto;

import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Data;

/**
 *
 * @author famillan
 */
@Data
@Schema(description = "This model represent a Employee data for openapi use on request methods")
public class EmployeeRequestDto {

    @Schema(name = "name", example = "Fabian", description = "Representes employee's name")
    private String name;

    @Schema(name = "salary", example = "1", description = "Representes employee's mounthly salary")
    private Integer salary;

    @Schema(name = "age", example = "1", description = "Representes how old is the employee")
    private Integer age;

    @Schema(name = "profileImage", example = "1", description = "Representes employee's proofile image uri")
    private String profileImage;

    @Schema(name = "anualSalary", example = "1", description = "Representes employee's anual salary")
    private Integer anualSalary;

}
