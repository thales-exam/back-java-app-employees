/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.thales.exam.employee.dto.mapper;

import com.thales.exam.employee.dto.Employee;
import com.thales.exam.employee.dto.EmployeeRequestDto;
import java.util.List;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.Mappings;
import org.mapstruct.factory.Mappers;

/**
 *
 * @author famillan
 */
@Mapper(componentModel = "spring")
public interface EmployeeRequestMapper {

    EmployeeRequestMapper INSTANCE = Mappers.getMapper(EmployeeRequestMapper.class);

    @Mappings({
        @Mapping(source = "employeeName", target = "name"),
        @Mapping(source = "employeeSalary", target = "salary"),
        @Mapping(source = "employeeAge", target = "age"),
        @Mapping(source = "employeeAnualSalary", target = "anualSalary")})
    EmployeeRequestDto entityToModelRequest(Employee entity);

    List<EmployeeRequestDto> entityListToModelRequestList(List<Employee> entityList);

    Employee modelRequestToEntity(EmployeeRequestDto request);

    List<Employee> modelRequestListToEntityList(List<EmployeeRequestDto> requestList);

}
