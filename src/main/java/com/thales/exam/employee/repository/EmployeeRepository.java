/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.thales.exam.employee.repository;

import com.thales.exam.employee.dto.Employee;
import java.io.IOException;
import java.util.List;

/**
 *
 * @author famillan
 */
public interface EmployeeRepository {

    List<Employee> getEmployees() throws IOException;

    Employee getEmployeeById(Long id) throws IOException;
}
