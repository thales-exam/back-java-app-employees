/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.thales.exam.employee.repository;

import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.thales.exam.employee.dto.Employee;
import com.thales.exam.employee.retrofit.EmployeeRetrofitImpl;
import com.thales.exam.employee.retrofit.ResponseBody;
import static com.thales.exam.employee.service.EmployeeServiceImpl.calculateAnualSalary;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Repository;
import retrofit2.Call;
import retrofit2.Response;

/**
 *
 * @author famillan
 */
@Repository
public class EmployeeRepositoryImpl implements EmployeeRepository {
    
    private static final int HTTP_LIMMIT_ATTEMPS = 15;

    @Override
    public List<Employee> getEmployees() throws IOException {
        int attemps = HTTP_LIMMIT_ATTEMPS;
        Response<ResponseBody> response;
        do {
            Call<ResponseBody> responseEmployes = EmployeeRetrofitImpl.getInstance().getService().getEmployees();
            response = responseEmployes.execute();

            attemps--;
            if (attemps == 0) {
                break;
            }

        } while (!(response.isSuccessful() && response.code() == HttpStatus.OK.value()));

        ResponseBody responseBody = response.body();
        if (responseBody == null || responseBody.getStatus() == null || !responseBody.getStatus().equals("success")) {
            return null;
        }

        JsonArray employeesJsonArray = responseBody.getData().getAsJsonArray();

        List<Employee> employees = new ArrayList<>();
        for (JsonElement eployeeJsonElement : employeesJsonArray) {
            JsonObject employeeJsonObject = eployeeJsonElement.getAsJsonObject();
            employees.add(this.jsonObjectToEmployee(employeeJsonObject));
        }

        return employees;
    }

    @Override
    public Employee getEmployeeById(Long id) throws IOException {
        int attemps = HTTP_LIMMIT_ATTEMPS;
        Response<ResponseBody> response;
        do {
            Call<ResponseBody> responseEmployes = EmployeeRetrofitImpl.getInstance().getService().getEmployeeById(id);
            response = responseEmployes.execute();
            attemps--;
            if (attemps == 0) {
                break;
            }
        } while (!(response.isSuccessful() && response.code() == HttpStatus.OK.value()));

        ResponseBody responseBody = response.body();

        if (responseBody == null || responseBody.getStatus() == null || !responseBody.getStatus().equals("success") || responseBody.getData().isJsonNull()) {
            return null;
        }

        return this.jsonObjectToEmployee(responseBody.getData().getAsJsonObject());
    }

    private Employee jsonObjectToEmployee(JsonObject employeeJsonObject) {
        Employee employee = new Employee();
        employee.setId(employeeJsonObject.get("id").getAsLong());
        employee.setEmployeeName(employeeJsonObject.get("employee_name").getAsString());
        employee.setEmployeeSalary(employeeJsonObject.get("employee_salary").getAsInt());
        employee.setEmployeeAge(employeeJsonObject.get("employee_age").getAsInt());
        employee.setProfileImage(employeeJsonObject.get("profile_image").getAsString());
        employee.setEmployeeAnualSalary(calculateAnualSalary(employee));
        return employee;
    }
}
