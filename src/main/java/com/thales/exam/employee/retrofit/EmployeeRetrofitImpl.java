/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.thales.exam.employee.retrofit;

import java.util.concurrent.TimeUnit;
import okhttp3.OkHttpClient;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 *
 * @author famillan
 */
public class EmployeeRetrofitImpl {

    private static final String BASE_URL = "http://dummy.restapiexample.com/api/v1/";
    private static Retrofit retrofit;
    private static EmployeeRetrofitImpl mInstance;

    private static final OkHttpClient okHttpClient = new OkHttpClient.Builder()
            .connectTimeout(7, TimeUnit.SECONDS)
            .writeTimeout(7, TimeUnit.SECONDS)
            .readTimeout(7, TimeUnit.SECONDS)
            .build();

    private EmployeeRetrofitImpl() {
        retrofit = new retrofit2.Retrofit.Builder()
                .client(okHttpClient)
                .baseUrl(BASE_URL)
                .addConverterFactory(GsonConverterFactory.create())
                .build();

    }

    public static synchronized EmployeeRetrofitImpl getInstance() {
        if (mInstance == null) {
            mInstance = new EmployeeRetrofitImpl();
        }
        return mInstance;

    }

    public IEmployeeRetrofit getService() {
        return retrofit.create(IEmployeeRetrofit.class);
    }
}
