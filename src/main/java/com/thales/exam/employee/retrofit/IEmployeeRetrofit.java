/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.thales.exam.employee.retrofit;

import static com.thales.exam.employee.utils.Util.EMPLOYEES_ENDPOINT;
import static com.thales.exam.employee.utils.Util.EMPLOYEE_ENDPOINT;
import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Path;

/**
 *
 * @author famillan
 */
public interface IEmployeeRetrofit {

    @GET(EMPLOYEES_ENDPOINT)
    Call<ResponseBody> getEmployees();

    @GET(EMPLOYEE_ENDPOINT)
    Call<ResponseBody> getEmployeeById(@Path("id") Long id);
}
