/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.thales.exam.employee.retrofit;

import com.google.gson.JsonElement;
import com.google.gson.annotations.SerializedName;
import lombok.Data;

/**
 *
 * @author famillan
 */
@Data
public class ResponseBody {

    @SerializedName("status")
    private String status;

    @SerializedName("data")
    private JsonElement data;

    @SerializedName("message")
    private String message;

}
