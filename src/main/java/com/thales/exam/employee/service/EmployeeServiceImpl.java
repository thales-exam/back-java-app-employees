/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.thales.exam.employee.service;

import com.thales.exam.employee.dto.Employee;
import com.thales.exam.employee.repository.EmployeeRepository;
import java.io.IOException;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 *
 * @author famillan
 */
@Service
public class EmployeeServiceImpl implements EmployeeService {

    @Autowired
    private EmployeeRepository repository;

    @Override
    public List<Employee> getEmployees() throws IOException {
        return repository.getEmployees();
    }

    @Override
    public Employee getEmployeeById(Long id) throws IOException {
        return repository.getEmployeeById(id);
    }

    public static Integer calculateAnualSalary(Employee employee) {
        return employee.getEmployeeSalary() * 12;
    }

}
