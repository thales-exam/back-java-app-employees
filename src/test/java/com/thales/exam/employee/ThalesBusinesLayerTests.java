/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.thales.exam.employee;

import com.thales.exam.employee.dto.Employee;
import com.thales.exam.employee.repository.EmployeeRepository;
import com.thales.exam.employee.repository.EmployeeRepositoryImpl;
import com.thales.exam.employee.service.EmployeeServiceImpl;
import static com.thales.exam.employee.service.EmployeeServiceImpl.calculateAnualSalary;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.Optional;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Nested;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.ValueSource;
import org.mockito.Mockito;

/**
 *
 * @author famillan
 */
public class ThalesBusinesLayerTests {

    private final EmployeeRepository realRepository = new EmployeeRepositoryImpl();

    @BeforeAll
    static void beforeAll() {
        System.out.println("Starting testing ...");
    }

    @Test
    @DisplayName("Testing get All real employees from dummy api vs employees from mock dummy data")
    void REAL_TEST_GET_EMPLOYEES_DUMMYAPI_VS_MOCK() throws IOException {
        List<Employee> realEmployeesList = realRepository.getEmployees();
        EmployeeRepository repository = Mockito.mock(EmployeeRepository.class);
        Mockito.when(repository.getEmployees()).thenReturn(mockGetAllEmployeesDummyData());
        List<Employee> mockEmployeesList = repository.getEmployees();

        Assertions.assertNotNull(realEmployeesList);
        Assertions.assertNotNull(mockEmployeesList);
        Assertions.assertEquals(realEmployeesList, mockEmployeesList);
    }

    @ParameterizedTest
    @DisplayName("Testing get one employee by Id from mock dummy data vs real dummy api data")
    @ValueSource(longs = {1L, 10L, 20L, 24L})
    void REAL_TEST_FIND_ONE_EMPLOYEE_DUMMYAPI_VS_MOCK(Long id) throws IOException {
        EmployeeRepository repository = Mockito.mock(EmployeeRepository.class);
        Mockito.when(repository.getEmployeeById(id)).thenReturn(mockFindByIdEmployeeDummyData(id));
        Employee mockEmployee = repository.getEmployeeById(id);

        Employee realEmployee = realRepository.getEmployeeById(id);

        Assertions.assertNotNull(realEmployee);
        Assertions.assertNotNull(mockEmployee);
        Assertions.assertEquals(realEmployee, mockEmployee);

    }

    @Test
    @DisplayName("Testing anual salary of real dummy data")
    void REAL_TEST_CALCULATE_ANUAL_SALARY_EMPLOYEES_FROM_DUMMY() throws IOException {
        List<Employee> realEmployeesList = realRepository.getEmployees();
        realEmployeesList.forEach(employee
                -> Assertions.assertTrue(employee.getEmployeeAnualSalary().equals(employee.getEmployeeSalary() * 12))
        );
    }

    @Test
    @DisplayName("Testing get All employees from mock dummy api data")
    void getAllEmployeesMockTest() throws IOException {
        EmployeeRepository repository = Mockito.mock(EmployeeRepository.class);
        Mockito.when(repository.getEmployees()).thenReturn(mockGetAllEmployeesDummyData());
        List<Employee> employees = repository.getEmployees();
        Assertions.assertNotNull(employees);
    }

    @ParameterizedTest
    @DisplayName("Testing get one employee by Id from mock dummy api data")
    @ValueSource(longs = {1L, 10L, 20L, 24L})
    void getExistsEmployeeByIdTest(Long id) throws IOException {
        EmployeeRepository repository = Mockito.mock(EmployeeRepository.class);
        Mockito.when(repository.getEmployeeById(id)).thenReturn(mockFindByIdEmployeeDummyData(id));
        Employee employee = repository.getEmployeeById(id);
        Assertions.assertNotNull(employee);
        Assertions.assertEquals(id, employee.getId());
    }

    @ParameterizedTest
    @DisplayName("Testing get one employee by Id that does not exitst from mock dummy api data")
    @ValueSource(longs = {29L, 34L, 43L, 100L})
    void getNOTExistsEmployeeByIdTest(Long id) throws IOException {
        EmployeeRepository repository = Mockito.mock(EmployeeRepository.class);
        Mockito.when(repository.getEmployeeById(id)).thenReturn(mockFindByIdEmployeeDummyData(id));
        Employee employee = repository.getEmployeeById(id);
        Assertions.assertNull(employee);
    }

    @DisplayName("Testing calculate anual salary one employee")
    @ParameterizedTest
    @ValueSource(ints = {7280200, 8000000, 6980000, 7550000})
    void calculateAnualSalaryText(int testSalary) {
        Integer salary = testSalary;
        Employee employee = new Employee();
        employee.setId(1L);
        employee.setEmployeeName("Fabian Millan");
        employee.setEmployeeAge(25);
        employee.setEmployeeSalary(salary);
        employee.setProfileImage("");
        Integer realAnualSalary = salary * 12;
        employee.setEmployeeAnualSalary(EmployeeServiceImpl.calculateAnualSalary(employee));
        Assertions.assertEquals(realAnualSalary, employee.getEmployeeAnualSalary());
    }

    private Employee mockFindByIdEmployeeDummyData(Long id) {
        Optional<Employee> optional = mockGetAllEmployeesDummyData()
                .stream()
                .filter(e -> Objects.equals(e.getId(), id))
                .findFirst();
        if (optional.isPresent()) {
            return optional.get();
        }
        return null;
    }

    private static List<Employee> mockGetAllEmployeesDummyData() {
        List<Employee> employeesDummy = new ArrayList<>();

        Employee employee1 = new Employee();
        employee1.setId(1L);
        employee1.setEmployeeName("Tiger Nixon");
        employee1.setEmployeeSalary(320800);
        employee1.setEmployeeAge(61);
        employee1.setProfileImage("");
        employee1.setEmployeeAnualSalary(calculateAnualSalary(employee1));
        employeesDummy.add(employee1);

        Employee employee2 = new Employee();
        employee2.setId(2L);
        employee2.setEmployeeName("Garrett Winters");
        employee2.setEmployeeSalary(170750);
        employee2.setEmployeeAge(63);
        employee2.setProfileImage("");
        employee2.setEmployeeAnualSalary(calculateAnualSalary(employee2));
        employeesDummy.add(employee2);

        Employee employee3 = new Employee();
        employee3.setId(3L);
        employee3.setEmployeeName("Ashton Cox");
        employee3.setEmployeeSalary(86000);
        employee3.setEmployeeAge(66);
        employee3.setProfileImage("");
        employee3.setEmployeeAnualSalary(calculateAnualSalary(employee3));
        employeesDummy.add(employee3);

        Employee employee4 = new Employee();
        employee4.setId(4L);
        employee4.setEmployeeName("Cedric Kelly");
        employee4.setEmployeeSalary(433060);
        employee4.setEmployeeAge(22);
        employee4.setProfileImage("");
        employee4.setEmployeeAnualSalary(calculateAnualSalary(employee4));
        employeesDummy.add(employee4);

        Employee employee5 = new Employee();
        employee5.setId(5L);
        employee5.setEmployeeName("Airi Satou");
        employee5.setEmployeeSalary(162700);
        employee5.setEmployeeAge(33);
        employee5.setProfileImage("");
        employee5.setEmployeeAnualSalary(calculateAnualSalary(employee5));
        employeesDummy.add(employee5);

        Employee employee6 = new Employee();
        employee6.setId(6L);
        employee6.setEmployeeName("Brielle Williamson");
        employee6.setEmployeeSalary(372000);
        employee6.setEmployeeAge(61);
        employee6.setProfileImage("");
        employee6.setEmployeeAnualSalary(calculateAnualSalary(employee6));
        employeesDummy.add(employee6);

        Employee employee7 = new Employee();
        employee7.setId(7L);
        employee7.setEmployeeName("Herrod Chandler");
        employee7.setEmployeeSalary(137500);
        employee7.setEmployeeAge(59);
        employee7.setProfileImage("");
        employee7.setEmployeeAnualSalary(calculateAnualSalary(employee7));
        employeesDummy.add(employee7);

        Employee employee8 = new Employee();
        employee8.setId(8L);
        employee8.setEmployeeName("Rhona Davidson");
        employee8.setEmployeeSalary(327900);
        employee8.setEmployeeAge(55);
        employee8.setProfileImage("");
        employee8.setEmployeeAnualSalary(calculateAnualSalary(employee8));
        employeesDummy.add(employee8);

        Employee employee9 = new Employee();
        employee9.setId(9L);
        employee9.setEmployeeName("Colleen Hurst");
        employee9.setEmployeeSalary(205500);
        employee9.setEmployeeAge(39);
        employee9.setProfileImage("");
        employee9.setEmployeeAnualSalary(calculateAnualSalary(employee9));
        employeesDummy.add(employee9);

        Employee employee10 = new Employee();
        employee10.setId(10L);
        employee10.setEmployeeName("Sonya Frost");
        employee10.setEmployeeSalary(103600);
        employee10.setEmployeeAge(23);
        employee10.setProfileImage("");
        employee10.setEmployeeAnualSalary(calculateAnualSalary(employee10));
        employeesDummy.add(employee10);

        Employee employee11 = new Employee();
        employee11.setId(11L);
        employee11.setEmployeeName("Jena Gaines");
        employee11.setEmployeeSalary(90560);
        employee11.setEmployeeAge(30);
        employee11.setProfileImage("");
        employee11.setEmployeeAnualSalary(calculateAnualSalary(employee11));
        employeesDummy.add(employee11);

        Employee employee12 = new Employee();
        employee12.setId(12L);
        employee12.setEmployeeName("Quinn Flynn");
        employee12.setEmployeeSalary(342000);
        employee12.setEmployeeAge(22);
        employee12.setProfileImage("");
        employee12.setEmployeeAnualSalary(calculateAnualSalary(employee12));
        employeesDummy.add(employee12);

        Employee employee13 = new Employee();
        employee13.setId(13L);
        employee13.setEmployeeName("Charde Marshall");
        employee13.setEmployeeSalary(470600);
        employee13.setEmployeeAge(36);
        employee13.setProfileImage("");
        employee13.setEmployeeAnualSalary(calculateAnualSalary(employee13));
        employeesDummy.add(employee13);

        Employee employee14 = new Employee();
        employee14.setId(14L);
        employee14.setEmployeeName("Haley Kennedy");
        employee14.setEmployeeSalary(313500);
        employee14.setEmployeeAge(43);
        employee14.setProfileImage("");
        employee14.setEmployeeAnualSalary(calculateAnualSalary(employee14));
        employeesDummy.add(employee14);

        Employee employee15 = new Employee();
        employee15.setId(15L);
        employee15.setEmployeeName("Tatyana Fitzpatrick");
        employee15.setEmployeeSalary(385750);
        employee15.setEmployeeAge(19);
        employee15.setProfileImage("");
        employee15.setEmployeeAnualSalary(calculateAnualSalary(employee15));
        employeesDummy.add(employee15);

        Employee employee16 = new Employee();
        employee16.setId(16L);
        employee16.setEmployeeName("Michael Silva");
        employee16.setEmployeeSalary(198500);
        employee16.setEmployeeAge(66);
        employee16.setProfileImage("");
        employee16.setEmployeeAnualSalary(calculateAnualSalary(employee16));
        employeesDummy.add(employee16);

        Employee employee17 = new Employee();
        employee17.setId(17L);
        employee17.setEmployeeName("Paul Byrd");
        employee17.setEmployeeSalary(725000);
        employee17.setEmployeeAge(64);
        employee17.setProfileImage("");
        employee17.setEmployeeAnualSalary(calculateAnualSalary(employee17));
        employeesDummy.add(employee17);

        Employee employee18 = new Employee();
        employee18.setId(18L);
        employee18.setEmployeeName("Gloria Little");
        employee18.setEmployeeSalary(237500);
        employee18.setEmployeeAge(59);
        employee18.setProfileImage("");
        employee18.setEmployeeAnualSalary(calculateAnualSalary(employee18));
        employeesDummy.add(employee18);

        Employee employee19 = new Employee();
        employee19.setId(19L);
        employee19.setEmployeeName("Bradley Greer");
        employee19.setEmployeeSalary(132000);
        employee19.setEmployeeAge(41);
        employee19.setProfileImage("");
        employee19.setEmployeeAnualSalary(calculateAnualSalary(employee19));
        employeesDummy.add(employee19);

        Employee employee20 = new Employee();
        employee20.setId(20L);
        employee20.setEmployeeName("Dai Rios");
        employee20.setEmployeeSalary(217500);
        employee20.setEmployeeAge(35);
        employee20.setProfileImage("");
        employee20.setEmployeeAnualSalary(calculateAnualSalary(employee20));
        employeesDummy.add(employee20);

        Employee employee21 = new Employee();
        employee21.setId(21L);
        employee21.setEmployeeName("Jenette Caldwell");
        employee21.setEmployeeSalary(345000);
        employee21.setEmployeeAge(30);
        employee21.setProfileImage("");
        employee21.setEmployeeAnualSalary(calculateAnualSalary(employee21));
        employeesDummy.add(employee21);

        Employee employee22 = new Employee();
        employee22.setId(22L);
        employee22.setEmployeeName("Yuri Berry");
        employee22.setEmployeeSalary(675000);
        employee22.setEmployeeAge(40);
        employee22.setProfileImage("");
        employee22.setEmployeeAnualSalary(calculateAnualSalary(employee22));
        employeesDummy.add(employee22);

        Employee employee23 = new Employee();
        employee23.setId(23L);
        employee23.setEmployeeName("Caesar Vance");
        employee23.setEmployeeSalary(106450);
        employee23.setEmployeeAge(21);
        employee23.setProfileImage("");
        employee23.setEmployeeAnualSalary(calculateAnualSalary(employee23));
        employeesDummy.add(employee23);

        Employee employee24 = new Employee();
        employee24.setId(24L);
        employee24.setEmployeeName("Doris Wilder");
        employee24.setEmployeeSalary(85600);
        employee24.setEmployeeAge(23);
        employee24.setProfileImage("");
        employee24.setEmployeeAnualSalary(calculateAnualSalary(employee24));
        employeesDummy.add(employee24);

        return employeesDummy;
    }
}
